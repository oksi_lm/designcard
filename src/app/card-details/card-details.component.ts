import { Component, OnInit } from '@angular/core';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-card-details', 
  templateUrl: './card-details.component.html', 
  styleUrls: ['./card-details.component.css'],
  providers: [NgbRatingConfig]
})

export class CardDetailsComponent implements OnInit {

  constructor(config: NgbRatingConfig) {
    config.max = 5;
    config.readonly = true;
   
  }

  ngOnInit(): void {
  }

}



