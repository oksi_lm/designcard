import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";


// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardDetailsComponent } from './card-details/card-details.component';
import { CardTitleComponent } from './card-title/card-title.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    CardDetailsComponent,
    CardTitleComponent
  ],
  imports: [
    BrowserModule,
    // AppRoutingModule
    RouterModule.forRoot([
      {path: "card-title", component: CardTitleComponent},
      {path: "card-details", component: CardDetailsComponent},
      {path: "", redirectTo: "card-title", pathMatch: "full"}
    ]),
    NgbModule
  ],
 
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
