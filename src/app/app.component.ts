import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'designCard';
  // topBackground:string = "asssets/photos/TopBackground.jpg";
  // background:string = "assets/photos/Background.jpg";
  // bankCard:string = "assets/photos/BankCard.jpg";
}
